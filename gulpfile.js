/* File: gulpfile.js */

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssMin = require('gulp-cssmin');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('css', function() {
  return gulp.src(['./sass/*.scss'])
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(cssMin())
      .pipe(autoprefixer())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./css'));
});

gulp.task('js', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './js/bootstrap.min.js',
        './js/custom.js'
    ])
    .pipe(concat('all.js'))
    .pipe(uglify()) 
    .pipe(gulp.dest('./js'));
});

gulp.task('watch', function() {
    gulp.watch(['./sass/*.scss'], ['css']);
    gulp.watch(['./js/*.js'], ['js']);
});

gulp.task('default', ['css', 'js', 'watch']);