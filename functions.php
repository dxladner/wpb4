<?php
/**
 * wpb4 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wpb4
 */
/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
if(!defined('WPB4_URL')) {
    define('WPB4_URL', get_template_directory_uri());
}

if(!defined('WPB4_DIR')) {
    define('WPB4_DIR', get_template_directory());
}

if ( !function_exists( 'wpb4_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpb4_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on wpb4, use a find and replace
		 * to change 'wpb4' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wpb4', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'wpb4' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wpb4_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wpb4_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpb4_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wpb4_content_width', 640 );
}
add_action( 'after_setup_theme', 'wpb4_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wpb4_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wpb4' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wpb4' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar(array (
		'name'		=> __('Footer One', 'wpb4'),
		'id'		=> 'footer-one-widget-area',
		'description'	=> __('The first footer widget area', 'dir'),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		)
	);
	register_sidebar(array (
		'name'		=> __('Footer Two', 'wpb4'),
		'id'		=> 'footer-two-widget-area',
		'description'	=> __('The second footer widget area', 'dir'),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		)
	);
	register_sidebar(array (
		'name'		=> __('Footer Three', 'wpb4'),
		'id'		=> 'footer-three-widget-area',
		'description'	=> __('The third footer widget area', 'dir'),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		)
	);
	register_sidebar(array (
		'name'		=> __('Bottom Widget Left', 'wpb4'),
		'id'		=> 'bottom-widget-left',
		'description'	=> __('The bottom left footer widget area', 'dir'),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		)
	);
	register_sidebar(array (
		'name'		=> __('Bottom Widget Right', 'wpb4'),
		'id'		=> 'bottom-widget-right',
		'description'	=> __('The bottom right footer widget area', 'dir'),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'wpb4_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wpb4_scripts() {
  wp_enqueue_style( 'wpb4-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );

	wp_enqueue_style( 'wpb4-style', get_stylesheet_uri() );

	wp_enqueue_script( 'bizsgvjs', get_template_directory_uri() . '/js/custom.js', array('jquery'), true );

	wp_enqueue_script( 'wpb4-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'wpb4-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpb4_scripts' );


// Register Userdata Custom Post Type
function userdata_custom_post_type() {

		$labels = array(
			'name'                  => _x( 'Userdata', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Userdata', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Userdata', 'text_domain' ),
			'name_admin_bar'        => __( 'Userdata', 'text_domain' ),
			'archives'              => __( 'Userdata Archives', 'text_domain' ),
			'attributes'            => __( 'Userdata Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Userdata:', 'text_domain' ),
			'all_items'             => __( 'All Userdata', 'text_domain' ),
			'add_new_item'          => __( 'Add New Userdata', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Item', 'text_domain' ),
			'edit_item'             => __( 'Edit Item', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'View Userdata', 'text_domain' ),
			'view_items'            => __( 'View Userdata', 'text_domain' ),
			'search_items'          => __( 'Search Userdata', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Userdata', 'text_domain' ),
			'items_list'            => __( 'Userdata list', 'text_domain' ),
			'items_list_navigation' => __( 'Userdata list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Userdata', 'text_domain' ),
			'description'           => __( 'Userdata Description', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( ),
			'taxonomies'            => array( 'category', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'userdata', $args );

	}
	add_action( 'init', 'userdata_custom_post_type', 0 );
/**
 * Bootstrap 4 Nav Walker
 */
 require get_template_directory() . '/inc/bs4navwalker.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
