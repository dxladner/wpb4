<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpb4
 */

?>

</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div id="pre-footer" class="row">
				<div class="col-sm-4 col-md-4 footer-widget">
					<?php
						if(is_active_sidebar('footer-one-widget-area')){
						dynamic_sidebar('footer-one-widget-area');
						}
					?>
				</div>
				<div class="col-sm-4 col-md-4 footer-widget">
					<?php
						if(is_active_sidebar('footer-one-widget-area')){
						dynamic_sidebar('footer-one-widget-area');
						}
					?>
				</div>
				<div class="col-sm-4 col-md-4 footer-widget">
					<?php
						if(is_active_sidebar('footer-one-widget-area')){
						dynamic_sidebar('footer-one-widget-area');
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6">
				<?php
					if(is_active_sidebar('bottom-widget-left')){
						dynamic_sidebar('bottom-widget-left');
					}
				?>	
				</div>
				<div class="col-lg-6 col-md-6">
				<?php
					if(is_active_sidebar('bottom-widget-right')){
						dynamic_sidebar('bottom-widget-right');
					}
				?>
				</div>
			</div><!-- .row -->
		</div><!-- .containr -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>
