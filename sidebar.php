<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpb4
 */

		if ( ! is_active_sidebar( 'sidebar-1' ) ) {
			return;
		}
		?>
		<div id="secondary" class="widget-area col-lg-3 col-md-3 col-sm-12 col-12" role="complementary">
			<div class="well">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div>
		</div><!-- #secondary -->
	</div>  <!-- close row -->
</div> <!-- close container -->
